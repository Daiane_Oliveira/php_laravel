<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\ModelCadastro;
use App\User;

class BookController extends Controller
{
    private $objUser;
    private $objCadastro;

    public function __construct()
    {
       // $this->objUser=new User();
        $this->objCadastro=new ModelCadastro();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cadastro = $this->objCadastro->all();
        return view("index",compact('cadastro'));
        //dd($this->objCadastro->find(1));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cad=$this->objCadastro->create([
            'nome'=>$request->nome,
            'email'=>$request->email,
            'telefone'=>$request->telefone,
            'endereço'=>$request->endereço,
            'Cidade'=>$request->Cidade

        ]);
        if($cad){
            return redirect('cadastros');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $cadastro=$this->objCadastro->find($id);
        return view('show', compact('cadastro'));
       //echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cadastro=$this->objCadastro->find($id);
        return view('create',compact('cadastro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->objCadastro->where(['id'=>$id])->update([
            'nome'=>$request->nome,
            'email'=>$request->email,
            'telefone'=>$request->telefone,
            'endereço'=>$request->endereço,
            'Cidade'=>$request->Cidade
        ]);
        return redirect('cadastros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cadastro->delete();
        return redirect('cadastros');
       
    }
}
