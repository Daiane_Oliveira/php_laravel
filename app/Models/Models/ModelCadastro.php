<?php

namespace App\Models\Models;

/*use Illuminate\Database\Eloquent\Factories\HasFactory;*/
use Illuminate\Database\Eloquent\Model;

class ModelCadastro extends Model
{
    protected $table = 'cadastro';
    protected $fillable = ['nome','email','telefone','endereço','Cidade'];
}
