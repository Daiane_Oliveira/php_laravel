@extends('templates.template')

@section('content')
    
    <form  method="delete" action="{{url("cadastros/$cadastro->id")}}">
    
        <input class="btn btn-danger" type="hidden" name="_method" value="DELETE">
    </form>

@endsection