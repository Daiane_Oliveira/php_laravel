@extends('templates.template')

@section('content')
    <h1 class="text-center">@if(isset($cadastro))Editar @else Cadastrar @endif</h1><hr>
    
    <div class="col-10 m-auto">
    @if(isset($cadastro))
        <form name="formEdit" id="formEdit" method="post" action="{{url("cadastros/$cadastro->id")}}">
        @method('PUT')
    @else

        <form name="formCad" id="formCad" method="post" action="{{url('cadastros')}}">
    @endif
            @csrf 
            <input class="form-control" type="text" name="nome" id="nome" placeholder="Nome: " value="{{$cadastro->nome ?? ''}}" required><br>
            <input class="form-control" type="text" name="email" id="email" placeholder="E-mail: " value="{{$cadastro->email ?? ''}}" required><br>
            <input class="form-control" type="text" name="telefone" id="telefone" placeholder="Telefone: " value="{{$cadastro->telefone ?? ''}}" required><br>
            <input class="form-control" type="text" name="endereço" id="endereço" placeholder="Endereço: " value="{{$cadastro->endereço ?? ''}}" required><br>
            <input class="form-control" type="text" name="Cidade" id="Cidade" placeholder="Cidade: " value="{{$cadastro->Cidade ?? ''}}" required><br>
            <input class="btn btn-primary" type="submit" value="@if(isset($cadastro))Editar @else Cadastrar @endif">
        </form>
    </div>
@endsection