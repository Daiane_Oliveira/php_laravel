@extends('templates.template')

@section('content')
    <h1 class="text-center">Crud</h1><hr>
    <div class="text-center mt-3 mb-4">
        <a href="{{url('cadastros/create')}}">
            <button class="btn btn-success">Cadastrar</button>
        </a>
    </div>
    <div class="col-10 m-auto">
        <table class="table table-dark text-center">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nome</th>
                <th scope="col">Cidade</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($cadastro as $cadastro)
                <tr class="table-active">
                    <th scope="row">{{$cadastro->id}}</th>
                    <td>{{$cadastro->nome}}</td>
                    <td>{{$cadastro->Cidade}}</td>
                    <td>
                        <a href="{{url("cadastros/$cadastro->id")}}">
                            <button class="btn btn-light">Visualizar</button>
                        </a>
                        <a href="{{url("cadastros/$cadastro->id/edit")}}">
                            <button class="btn btn-primary">Editar</button>
                        </a>
                        <a href="{{url("cadastros/$cadastro->id/detroy")}}">
                            <button class="btn btn-danger">Excluir</button>
                        </a>
                    </td>
                </tr>
               
                @endforeach
            
            </tbody>
        </table>
    </div>
@endsection